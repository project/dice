<?php

/**
 * @file
 * The dice main setting.
 *
 * main settings for dice
 */

function dice_settings() {
  $form['dice_active'] = array(
    '#type' => 'checkbox',
    '#title' => t( 'Enable ICE' ),
    '#description' => t( 'If enabled, ICE is active' ),
    '#default_value' => variable_get( 'dice_active', 1 ),
  );
  
  $form['dice_profile'] = array(
    '#type' => 'textfield',
    '#title' => t( 'ICE Profile' ),
    '#description' => t( 'Leave it blank for default Profile' ),
    '#default_value' => variable_get( 'dice_profile', '' ),
  );
  
  return system_settings_form( $form );
}